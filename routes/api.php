<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('settings',[ApiController::class, 'settings'])->name('settings');
Route::get('blogs',[ApiController::class, 'blogs'])->name('blogs');
Route::get('blogsList',[ApiController::class, 'blogsList'])->name('blogsList');
Route::get('blog/{slug}',[ApiController::class, 'blog'])->name('blog');

Route::get('events',[ApiController::class, 'events'])->name('events');
Route::get('event',[ApiController::class, 'event'])->name('event');





Route::post('send-contact',[ApiController::class, 'contact'])->name('contact');
