import Swal from "sweetalert2";

export default msg => {
  Swal.fire({
    title: 'Sucesso!',
    text: typeof msg !== 'undefined' ? msg : 'Operação realizada com sucesso.',
    type: 'success',
    icon: 'success',
    timer: 3000
  })
}
