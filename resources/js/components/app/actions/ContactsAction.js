import Config from '../config'

const PLURAL = 'CONTATOS'
const SINGULAR = 'CONTATO'
const LIST_URL = '/contatos'


export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    }
}


export const sendContato = (method, request) => {

    // console.log(request)
    return dispatch => {
        dispatch(unset())
        Config.add(method,request,dispatch,SINGULAR)
    }
}

