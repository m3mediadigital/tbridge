import Config from '../config'
const PLURAL = "EVENTS";
const SINGULAR = "EVENT";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};


export const listEvents = () => {
    return dispatch => {
        dispatch(unset());
        Config.list("events", dispatch, `SET_${PLURAL}`);
    };
};

export const listEventsList = () => {
    return dispatch => {
        dispatch(unset());
        Config.list("events", dispatch, `SET_${PLURAL}`);
    };
};

export const getEvent = (slug) => {
    return dispatch => {
        dispatch(unset());
        Config.get("event", slug, dispatch, `SET_${SINGULAR}`);
    };
};