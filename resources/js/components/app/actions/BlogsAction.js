import Config from '../config'
const PLURAL = "BLOGS";
const SINGULAR = "BLOG";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};


export const listBlogs = () => {
    return dispatch => {
        dispatch(unset());
        Config.list("blogs", dispatch, `SET_${PLURAL}`);
    };
};

export const listBlogsList = () => {
    return dispatch => {
        dispatch(unset());
        Config.list("blogs", dispatch, `SET_${PLURAL}`);
    };
};

export const getBlog = (slug) => {
    return dispatch => {
        dispatch(unset());
        Config.get("blog", slug, dispatch, `SET_${SINGULAR}`);
    };
};