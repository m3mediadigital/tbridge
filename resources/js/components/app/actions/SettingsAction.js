import Config from '../config'
const PLURAL = "SETTINGS";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};


export const getSettings = () => {
    return dispatch => {
        dispatch(unset());
        Config.list("settings", dispatch, `SET_${PLURAL}`);
    };
};