import { combineReducers } from 'redux'

import SettingsReducer  from './reducers/SettingsReducer'
import ContactsReducer  from './reducers/ContactsReducer';
import BlogsReducer from './reducers/BlogsReducer';
import EventsReducer from './reducers/EventsReducer';

export default combineReducers({
    settings: SettingsReducer ,
    contact: ContactsReducer ,
    blogs: BlogsReducer,
    events: EventsReducer
})