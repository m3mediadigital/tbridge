import api from './api'
import fireError from './fireError'
import fireSuccess from './fireSuccess'

const get = (method, slug, dispatch, singular) => {
    api.get(method + "/" + slug)
        .then(response => {
            dispatch({
                type: singular,
                payload: response.data
            });
        })
        .catch(error => {
            console.log(error);
        });
};

const list = (method, dispatch, plural) => {
    api.get(method)
        .then(response => {
            dispatch({
                type: plural,
                payload: response.data
            });
        })
        .catch(error => {
            console.log(error);
        });
};

const add = (method, form, dispatch, singular) => {
    api.post(method, form,{
        headers:{
            'Content-Type': 'multipart/form-data'
        }
    })
    .then(response => {
        
        if(response.data.error){
            fireWarning(response.data.error)
            console.log(response.data.warning)

        }else if(response.data.success){
            fireSuccess(response.data.success)

        }
    })
    .catch(error => {
        fireError()
    })
}

export default { get, list, add };