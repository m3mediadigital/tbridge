import {fromJS} from 'immutable'

const PLURAL = 'BLOGS'
const SINGULAR = 'BLOG'

const INITIAL_STATE = fromJS({
  list_blogs: [],
  meta: [],
  item_blog: null,
  error_contato: [],
  success_contato: []
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_blogs', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_blog', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_blog', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_blogs', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_blog', fromJS([]))
    default:
      return state
  }
}