import {fromJS} from 'immutable'

const PLURAL = 'EVENTS'
const SINGULAR = 'EVENT'

const INITIAL_STATE = fromJS({
  list_events: [],
  meta: [],
  item_event: null,
  error_contato: [],
  success_contato: []
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_events', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_event', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_event', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_events', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_event', fromJS([]))
    default:
      return state
  }
}