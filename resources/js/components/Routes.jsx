import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import Part from './pages/Part';
import OurBlog from './pages/OurBlog';
import Article from './pages/Article';
import Eventos from './pages/Eventos';
import Blog from './pages/Blog';

export default (props) => {
	return (
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/faca-parte" component={Part} />
				<Route path="/blogs" component={OurBlog} />
				<Route path="/artigo" component={Article} />
				<Route path="/eventos" component={Eventos} />
				<Route path="/blog/:slug" component={Blog} />
			</Switch>
		</BrowserRouter>
	);
} 
