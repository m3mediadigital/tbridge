import Category from '../../fragments/Category';
import './styles.scss';
import { Link } from 'react-router-dom';

const CardProvisorio2 = () => (
    <div className="cardSustainability">
      {/* <Link to="/interna-blog" className=""> */}
        <Category
            backgroundColor="#A0045E"
            text="Sustentabilidade"
        />
        <h3 className="cardSustainability__h3">Aguarde Novos Eventos <br/>.<br/>.</h3>
        <div clasName="">
            <p className="cardSustainability__description" style={{ color: "#fff" }}>
                Não pessoal; não vou “resenhar”
                sobre o aclamado filme de 1975 do
                diretor Miloš Forman, protagonizado pelo
                insuperável “coringa de plantão” Jack Nicholson.
            </p>
        </div>
    {/* </Link> */}
    </div>
);

export default CardProvisorio2;
