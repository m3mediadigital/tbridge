import Category from '../../fragments/Category';
import './styles.scss';
import { Link } from 'react-router-dom';

const CardSustainability = () => (
  <div className="cardSustainability">
        <a href="/tbridge-uma-estranha-mas-necessaria-no-ninho" className="text-decoration-none">
            <Category text="Ecossistema" backgroundColor="#77B926" />
            <h3 className="cardSustainability__h3">TBridge: uma estranha (mas necessária) no ninho</h3>
            <p className="cardSustainability__description">
                Não pessoal; não vou “resenhar” sobre o aclamado
                filme de 1975 do diretor Miloš Forman, protagonizado
                pelo insuperável “coringa de plantão” Jack Nicholson.
            </p>
        </a>
  </div>
);

export default CardSustainability;
