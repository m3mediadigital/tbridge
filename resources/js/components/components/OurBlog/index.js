import { Container, Row, Col } from 'react-bootstrap';

import Category from '../../fragments/Category';

import { Link, useLocation } from 'react-router-dom';

import './styles.scss';

const OurBlogComponent = ({items}) => {

	return(
		<div className="OurBlog" id="NossoBlogInterna">
			<Container>
				<Row>
					<Col sm={12} lg={12}>
						<h1 className="OurBlog__title">Nosso Blog</h1>
					</Col>
				</Row>
				{
					items.map((item, key) => {
						return (
							<Row key={key}>
								<a href={`/blog/${item.get('slug')}`} className="text-decoration-none">
									<Col sm={12} lg={12}>
										<div className="OurBlog__post">
											<Category text={item.getIn(['categorys', 'title'])} backgroundColor={item.getIn(['categorys', 'color'])} />
											<h2 className="OurBlog__post--title">{ item.get('title') }</h2>
											<p className="OurBlog__post--description">{ item.get('seo') }</p>
										</div>
									</Col>
								</a>
							</Row>
						)
					})
				}
			</Container>
		</div>
	);
}

export default OurBlogComponent;
