import { Container, Row, Col } from 'react-bootstrap';
import Category from '../../fragments/Category';
import './styles.scss';

const InternaBlog = ({item}) => {
	return(
		<div className="OurBlog pt-5" id="NossoBlogInterna">
			<Container>
				<Row>
					<Col sm={12} lg={12}>
					</Col>
				</Row>
				<Row>
					<Col sm={12} lg={12}>
						<div className="OurBlog__post pt-5">
							<h1 className="OurBlog__title">{item.title}</h1>
							<div className="row categoria-interna d-flex pt-3">
								{item.categorys && (
									<div className="ml-3">
										<Category text={item.categorys.title} backgroundColor={item.categorys.color} />
									</div>
								)}
								{/* {item.autor && (
									<div className="mt-1">
										<p className="ml-3">Autor: {item.autor}</p>
									</div>
								)} */}
							</div>
                            <div className="pt-4">
                                {item.files && (
                                    <img className="img-fluid" src={`/${item.files.path}`}  alt={item.title} />
                                )}
                            </div>
							<div className="OurBlog__post--description" dangerouslySetInnerHTML={{__html: item.description}}></div>
							{item.autor && (
								<p className="OurBlog__post--description">
									Fonte: <a href={item.autor} target="_blank" >{item.autor}</a>
								</p>
							)}
						</div>
					</Col>
				</Row>
			</Container>
		</div>
	);
}

export default InternaBlog;
