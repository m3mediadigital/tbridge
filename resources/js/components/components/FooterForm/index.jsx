import React from 'react'

import { Container, Row, Col } from 'react-bootstrap';
import { GrInstagram, GrLinkedinOption } from 'react-icons/gr';
import { ImWhatsapp } from 'react-icons/im';
import { AiOutlineYoutube } from 'react-icons/ai';
import { RiSpotifyLine } from 'react-icons/ri';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import { sendContato } from '../../app/actions/ContactsAction';
import './styles.scss';

class FooterForm extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            name: "",
            phone: "",
            email: "",
			message: "",
            buttonEnabled: true,
            buttonText: "Enviar",
        };

		this.method = "send-contact"
        this.handleFielsChange = this.handleFielsChange.bind(this);
        this.handleSendMail = this.handleSendMail.bind(this);
        this.clearForm = this.clearForm.bind(this);
    }

    handleFielsChange(event) {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    }

    handleSendMail(event) {
        event.preventDefault();
        this.setState({ buttonEnabled:false, buttonText: "Aguarde" })

		const formData = new FormData()
		formData.append('name', this.state.name)
		formData.append('phone', this.state.phone)
		formData.append('email', this.state.email)
		formData.append('message', this.state.message)

		this.props.sendContato(this.method, formData)
		this.clearForm()
    }

    clearForm() {
        this.setState({
            name: "",
            phone: "",
            email: "",
			message: ""
        })
    }

	render(){
		return(
			<Container id="faleConosco">
				<Row>
					<Col sm={12} lg={12}>
						<div className="containerForm" id="SpeakWithUs">
							<div className="conteinarForm containerForm__description">
								<h3 className="containerForm__description--h3">
								Vamos conversar
								<br />
								um pouco?
								</h3>
							</div>
						</div>
					</Col>
				</Row>
				<Row>
					<Col sm={12} lg={7}>
						<form className="containerForm" onSubmit={this.handleSendMail}>
							<p className="containerForm__description--p containerForm__description">
								Utilize o formulário para entrar em contato conosco.
								Será um enorme satisfação responder.
							</p>
							<div className="inputForm">
								<input type="text" name="name" required value={this.state.name} onChange={this.handleFielsChange} />
								<label htmlFor="email">Seu Nome</label>
							</div>

							<div className="inputForm">
								<input type="tel" name="phone" required value={this.state.phone} onChange={this.handleFielsChange} />
								<label htmlFor="email">Seu Telefone</label>
							</div>
							<div className="inputForm">
								<input type="email" name="email" required  value={this.state.email} onChange={this.handleFielsChange} />
								<label htmlFor="email">Seu Email</label>
							</div>

							<div className="inputForm">
								<textarea type="text" name="message" required rows={4} cols={15} value={this.state.message} onChange={this.handleFielsChange} />
								<label htmlFor="email">Sua mensagem</label>
							</div>
							<div className="d-flex justify-content-end simple__adjust">
								<button type="submit" className="buttonDefault" disabled={!this.state.buttonEnabled}>
                                	{this.state.buttonText}
								</button>
							</div>
						</form>
					</Col>
					<Col sm={12} lg={5}>
						<div className="containerInfo">
							<div className="containerInfo__address">
								<strong>Endereço:</strong>
								<p>Rua Gen. Felizardo Brito, 2936 - Capim Macio, Natal - RN, 59078-410</p>
							</div>
							<div className="containerInfo__cell">
								<strong>Telefone:</strong>
								<a href="https://api.whatsapp.com/send?phone=558498113-0303&text=Oi!" className="text-decoration-none">
									<p className="text-decoration-none" >(84) 98113-0303</p>
								</a>
							</div>
							<div className="containerSocial">
								<strong>Nossas Redes:</strong>
								<ul className="containerSocial__lists">
									<li>
										<a href="https://www.instagram.com/tbridge.co/?hl=pt-br" target="_blank" rel="noreferrer">
											<GrInstagram size={32} color="#FFFFFF" />
										</a>
									</li>
									<li className="containerSocial__lists--linkedin">
										<a href="https://www.linkedin.com/company/tbridge-co" target="_blank" rel="noreferrer">
											<GrLinkedinOption size={26} color="#FFFFFF" />
										</a>
									</li>
									<li>
										<a href="https://api.whatsapp.com/send?phone=5584981130303&text=Oi!" target="_blank" rel="noreferrer">
											<ImWhatsapp size={32} color="#FFFFFF" />
										</a>
									</li>
									<li>
										<a href="https://www.youtube.com/channel/UCQxVast3Jch47CrLIGIMG7A" target="_blank" rel="noreferrer">
											<AiOutlineYoutube size={42} color="#FFFFFF" />
										</a>
									</li>
									{/* <li>
										<a href="https://google.com" target="_blank" rel="noreferrer">
											<RiSpotifyLine size={38} color="#FFFFFF" />
										</a>
									</li> */}
								</ul>
							</div>
						</div>
					</Col>
				</Row>
			</Container>
		)
	}
}

const mapDispatchToProps = (dispatch) => bindActionCreators({sendContato}, dispatch)

export default connect(null, mapDispatchToProps) (FooterForm);
