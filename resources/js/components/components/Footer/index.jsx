import React, {Component} from 'react'
import { Container, Row, Col, Carousel } from 'react-bootstrap';

import { BsArrowRight } from 'react-icons/bs';
import FooterForm from '../FooterForm';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { listBlogs } from '../../app/actions/BlogsAction';
import './styles.scss';
import Category from '../../fragments/Category';
import styled from 'styled-components';


const CardBlog = styled.div`
	background: #FFFFFF;
	padding: 25px 20px 35px 25px;
	font-family: 'Lexend';
	font-size: 18px !important;
	box-shadow: 0px 0px 12px #0000001F;
	margin-bottom: 5px;
	
	.cardSustainability__h3{
		color: #121212;
		text-align: left;
		font-weight: 900;
		font-size: 24px;
		letter-spacing: 0px;
		line-height: 34px;

		margin: 12px 0 0 0;
	}

	.cardSustainability__description{
		color: #60646C;
		font-size: 18px;
		line-height: 32px;

		margin: 9px 0 0 0;
		text-align: left;
	}
`


class Footer extends Component{
	constructor(props){
		super(props)

		this.listBlogs()
	}

	listBlogs(){
		this.props.listBlogs()
	}

	render(){
		
		return (
			<Container fluid id="NossoBlog">
				{
					this.props.list_blogs !== null && this.props.list_blogs.size === 0 ? false :
					this.props.list_blogs && (<ListBlogs items={this.props.list_blogs} /> )
				}
				<Row>
					{/* <StartupsLogos /> */}
				</Row>
				<Row className="containerFooterForm">
					<FooterForm settings={this.props.settings} />
				</Row>
			</Container>
		)
	}
}

const ListBlogs = ({items}) =>{
	return (
		<> 
			{ location.pathname !== '/blogs' && 
				<div>
					<Row className="footerCard">
						<div className="footerCard__content">
							<Container>
								<Row>
									<Col sm={12}>
										<h2 className="footerCard__content--h2">Novidades? Acompanhe nosso blog</h2>
									</Col>
								</Row>
							</Container>
						</div>
					</Row>
					<Row className="footerCardMobile d-sm-flex d-lg-none">
						<Container>
							<Row>
								<Carousel indicators={false} controls={false}>
									{
										items.map((item, key) => {
											return (
												<Col lg={12} key={key}>
													<CardBlog>
														<a href={`/blog/${item.get('slug')}`} className="text-decoration-none">
															<Category text={item.getIn(['categorys', 'title'])} backgroundColor={item.getIn(['categorys', 'color'])} />
															<h3 className="cardSustainability__h3">{ item.get('title') }</h3>
															<div className="cardSustainability__description" dangerouslySetInnerHTML={{__html: item.get('seo')}}></div>
														</a>
													</CardBlog>
												</Col>
											)
										})
									}
								</Carousel>
							</Row>
						</Container>
					</Row>
					<Row className="footerCardDesktop d-none d-lg-flex">
						<Container>
							<Row>
								{
									items.map((item, key) => {
										return (
											<Col lg={4} key={key}>
												<CardBlog>
													<a href={`/blog/${item.get('slug')}`} className="text-decoration-none">
														<Category text={item.getIn(['categorys', 'title'])} backgroundColor={item.getIn(['categorys', 'color'])} />
														<h3 className="cardSustainability__h3">{ item.get('title') }</h3>
														<div className="cardSustainability__description" dangerouslySetInnerHTML={{__html: item.get('seo')}}></div>
													</a>
												</CardBlog>
											</Col>
										)
									})
								}
							</Row>
						</Container>
					</Row>
					<Row>
						<Container>
							<Row>
								<Col sm={12} lg={12}>
									<div className="footerCard__containerButton">
									<a href="/blogs" className="footerCard__containerButton--buttonViewMore">
										Ver mais
										{' '}
										<BsArrowRight size={32} color="#121212" />
									</a>
									</div>
								</Col>
							</Row>
						</Container>
					</Row>		
				</div>
			}
		</>
	)
}

const mapStateToProps = state => ({
    list_blogs: state.blogs.get('list_blogs')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        listBlogs
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Footer)
