import { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import { HiMenu } from 'react-icons/hi';

import './styles.scss'

const Navbar = () => {
	const [activeColor, setActiveColor] = useState(false);
	const [isOpen, setIsOpen] = useState(false);

	const dispatch = useDispatch();
	const location = useLocation();

	function handleOpenMenu() {
		setIsOpen(!isOpen);
	}

	useEffect(() => {
		function handleScroll() {
			if (window.scrollY > 10 || location.pathname !== '/') {
				setActiveColor(true);
			} else {
				setActiveColor(false);
			}
		}

		window.addEventListener('scroll', handleScroll);
	}, [window.scrollY]);

	useEffect(() => {
		if (location.pathname !== '/') {
			setActiveColor(true);
		}
	}, []);

	return (
		<>
			<Container fluid className={`${isOpen && 'd-none'} ${activeColor && 'activeColor'} fixed-top py-3`}>
				<Row>
					<Container>
						<Row>
							<Col lg={12}>
								<nav className="menu">
									<button type="button" onClick={handleOpenMenu} className="menu__buttonMobile">
										<HiMenu size="32" color={`${!activeColor ? '#FFFFFF' : '#121212'}`} />
									</button>
									<div className="menu__brand">
										<a href="/">
											<span className="title">tbridge.</span>
											co
										</a>
									</div>
									<div className="menu__content">
										<li className="menu__list">
											<a href="/" className="menu__list--link">Home</ a>
										</li>
										<li className="menu__list">
											<a href="/faca-parte" className="menu__list--link">Faça Parte</a>
										</li>
										<li className="menu__list">
											<a href="/eventos" className="menu__list--link">Eventos</a>
										</li>
										<li className="menu__list">
											<a href="/blogs" className="menu__list--link">Blog</a>
										</li>
										<li className="menu__list">
											<a href="/#SpeakWithUs" className="menu__list--link">Fale Conosco</a>
										</li>
									</div>
								</nav>
							</Col>
						</Row>
					</Container>
				</Row>
			</Container>
			<nav className={`menuMobile ${isOpen && 'd-flex justify-content-center'}`}>
				<div className="menu__buttonMobile">
					<button type="button" onClick={handleOpenMenu} className="menu__buttonMobile--button position-absolute">
						<div className="menu__buttonMobile--one" />
						<div className="menu__buttonMobile--two" />
					</button>
				</div>
				<div className="menuMobile__content">
					<li className="menu__list">
						<Link to="/" className="menu__list--link" onClick={handleOpenMenu}>Home</Link>
					</li>
					<li className="menu__list">
						<Link to="/faca-parte" className="menu__list--link" onClick={handleOpenMenu}>Faça Parte</ Link>
					</li>
					<li className="menu__list">
						<Link to="/eventos" className="menu__list--link" onClick={handleOpenMenu}>Eventos</Link>
					</li>
					<li className="menu__list">
						<Link to="/blogs" className="menu__list--link" onClick={handleOpenMenu}>Blog</Link>
					</li>
					<li className="menu__list">
						<a className="menu__list--link" href="/#SpeakWithUs" onClick={handleOpenMenu}>Fale Conosco</a>
						{/* <Link to="#SpeakWithUs" className="menu__list--link" >Fale Conosco</Link> */}
					</li>
				</div>
			</nav>
		</>
	);
};

export default Navbar;
