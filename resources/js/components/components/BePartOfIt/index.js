import { Container, Row, Col } from 'react-bootstrap';

import Slider from "react-slick";
import styled from 'styled-components'
import './styles.scss';

const ComponentImg = styled.div`
	background-image: url('${props => props.Value }');
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
    height: 600px;
    width: 100%;

	@media(max-width: 768px){
		height: 300px;
	}
`

const settings = {
	dots: true,
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay: true,
	speed: 500,
	autoplaySpeed: 5000,
	cssEase: "linear",
	pauseOnHover: true
};

const Imgs = [
	{img: '/images/about/1.png'},
	{img: '/images/about/2.png'},
	{img: '/images/about/3.png'},
	{img: '/images/about/4.png'}
]	


const BePartOfIt = () => (
	<Container id="somos-o-tbriget">
		<Row>
			<Col sm={12} lg={12}>
				<div>
				<h3 className="title__h3">Somos o Tbridge</h3>
				</div>
			</Col>
		</Row>
		<Row>
			<Col lg={12}>
				<div className="BePartOfIt__simple__adjust">
					<Row>
						<Col sm={12} lg={6}>
							<div className="container__content">
								<p>
								Um ecossistema que ajuda e auxilia a projetos inovadores e promissores,
								oferencendo recursos intelectuais, financeiros e operacionais às startups.

								</p>
							</div>
						</Col>
						<Col sm={12} lg={6}>
							<div className="container__content">
								<p>
								Fisicamente nós estamos numa área de quase 2000 m², num ambiente extremamente
								inovador, onde aquela mente inquieta, aquela ideia disruptiva vai conseguir chegar ao próximo nível.
								</p>
							</div>
						</Col>
					</Row>
				</div>
			</Col>
		</Row>
		<Slider {...settings}>
			{Imgs.map((item, key) => {
				return(
					<div key={key}>
						<div key={key}>
							<a  data-fancybox="gallery" href={item.img}>
								<ComponentImg Value={item.img}>
									<img src={item.img} className="d-none" />
								</ComponentImg>
							</a>
						</div>
					</div>
				)
			})}	
		</Slider>
	</Container>
);

export default BePartOfIt;
