import { Container, Row, Col, Carousel } from 'react-bootstrap';
import Event from '../../assets/img/talkAndBeer.jpeg';
import './styles.scss';
import Card from '../Card'

const CheckTheSchedule = _ => (
    <Container>
        <Row>
            <Col sm={12} lg={6} className="CheckTheSchedule pl-0 pr-0">
                <Card text="Veja como foi nossos eventos anteriores" caminho="/eventos" color="#2E34DB" />
            </Col>
             {/* <Col sm={12} lg={6} className="CarouselEvent">
               <Carousel controls={false}>
                    <Carousel.Item className="carrousel-indicators">
                        <a href="https://forms.gle/HxExyC1BM8XtdEob6" className="text-decoration-none">
                            <img className="w-100" src={Event} alt="Talk and Beer" />
                        </a>
                    </Carousel.Item>
                </Carousel>
            </Col> */}
            <Col sm={12} lg={6} className="CheckTheSchedule pl-0 pr-0">
                <Card text="Garanta sua vaga nos nossos prox. eventos" caminho="#faleConosco" color="#121212" />
            </Col>
        </Row>
    </Container>
);

export default CheckTheSchedule;
