import {
  Container, Row, Col
} from 'react-bootstrap';

import './styles.scss';

const InternaEventos = ({events}) => {
	return(			
		<Row>
			<a href={`evento/${events.get('slug')}`}>
				<Col sm={12} lg={12}>
					<div className="OurBlog__post">
						<h2 className="OurBlog__post--title">Talk and Beer</h2>
						<div className="OurBlog__post--description" dangerouslySetInnerHTML={{__html: events.get('content')}}></div>
						{events.get('link') &&
							(
								<p className="OurBlog__post--description">
									Então <a href={events.get('link')} >clique aqui</a> e garanta sua vaga, mas corre que as inscrições são limitadas!
								</p>
							)
						}										
					</div>
				</Col>
			</a>
		</Row>
	)
}	

export default InternaEventos;