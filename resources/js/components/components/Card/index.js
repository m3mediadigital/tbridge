import { Col } from 'react-bootstrap';

const Card = (props) => (
    <a href={props.caminho} className="menu__list--link text-decoration-none">
        <div className="CheckTheSchedule__title d-flex justify-content-center align-items-center" id="CheckTheSchedule" style={{ backgroundColor: props.color }}>
            <h3 className="CheckTheSchedule__title--h3 pl-4">{ props.text }</h3>
        </div>
    </a>
);

export default Card;
