import { Container, Row, Col } from 'react-bootstrap';

import Chair from '../Chair';

import './styles.scss';

const KnowPlans = () => (
	<Container>
		<Row className="KnowPlans__line">
			<Col lg={12} className="KnowPlans">
				<h3 className="KnowPlans__h3">Conheça nosso espaço</h3>
			</Col>
			<Col lg={3}>
				<Chair color="#EF7C31"
					title="Zona Laranja"
					price="1040,00"
					type="mês"
					ponto1="Coworking com 6 estações de trabalho"
					ponto2="Estúdio"
					ponto3="Sala de Reunião"
				/>
			</Col>
			<Col lg={3}>
				<Chair
					color="#2E34DB"
					title="Zona Azul"
					price="40,00"
					type="hora"
					ponto1="Salas para startups com 12, 9 e 8 lugares"
					ponto2="Área de convivência"
				/>
			</Col>
			<Col lg={3}>
				<Chair
					color="#A0035E"
					title="Zona Magenta"
					price="180,00"
					type="diária"
					ponto1="Salas para startups com 6 , 7 e 8 lugares"
				/>
			</Col>
			<Col lg={3}>
				<Chair
					color="#77B926"
					title="Zona Verde"
					price="2990,00"
					type="hora"
					ponto1="Salas para startups com 4 e 7 lugares"
					ponto2="Área de convivência"
				/>
			</Col>
			<Col lg={3}>
				<Chair
					color="#8D1AF7"
					title="Zona Roxa"
					price="5490,00"
					type="hora"
					ponto1="Cozinha comunitária com refeitório"
					ponto2="Vestiário"
					ponto3="Guarda-volume"
				/>
			</Col>
			<Col lg={3}>
				<Chair
					color="#FDC429"
					title="Zona Amarela"
					price="40,00"
					type="hora"
					ponto1="Coworking com 22 estações de trabalho"
				/>
			</Col>
			<Col lg={3}>
				<Chair
					color="#1EA1B7"
					title="Zona Turqueza"
					price="40,00"
					type="hora"
					ponto1="Auditório com 20 lugares"
				/>
			</Col>
			<Col lg={3}>
				<Chair
					color="gray"
					title="Zona Externa"
					price="40,00"
					type="hora"
					ponto1="Área externa para eventos de até 60 pessoas"
					ponto2="Laboratório;"
					ponto3="Loja/Mercado"
				/>
			</Col>
		</Row>
	</Container>
);

export default KnowPlans;
