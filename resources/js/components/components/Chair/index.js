import ButtonDefault from '../../fragments/ButtonDefault';
import styled from 'styled-components'

import './styles.scss';


const Item = styled.li`
    &::before{
        content: '•';
        opacity: 1;
        color: ${(props) => props.ColorLi};
        widht: 10px !important;
        height: 14px !important;
        border-radius: 10px;
        margin-right: 3px;
    }
`

const Chair = ({ color, title, price, type, ponto1, ponto2, ponto3}) => {

    return(
        <div className="cart">
            <div className="cart__header" style={{ backgroundColor: color }}>
            <h4 className="cart__header--h4">{title}</h4>
            </div>
            <div className="cart__description" style={{ borderColor: color }}>
            {/* <span>O plano inclui:</span> */}
            <ul className="cart__description--planList">
                <Item ColorLi={color}>
                    {ponto1}
                </Item>

                {ponto2 &&(
                    <Item ColorLi={color}>

                        {ponto2}
                    </Item>
                )}

                {ponto3 &&(
                    <Item ColorLi={color}>

                        {ponto3}
                    </Item>
                )}
            </ul>
            </div>
            <div className="cart__price">
            {/* <strong>
                Preço por
                {' '}
                {type}
                :
            </strong> */}
            {/* <h5 style={{ color }}>
                R$
                {' '}
                {price}
            </h5> */}
            <a href="#formplano">
                <ButtonDefault text="Gostei" color={color} />
            </a>
            </div>
        </div>
    );
}

export default Chair;
