import { Container, Row, Col } from 'react-bootstrap';

import Bucado from '../../assets/img/LogoBucadoPretoVerde.png';
import Iwof from '../../assets/img/LogoIwof.png';

import './styles.scss';

const StartupsLogos = _ => (
    <Container>
        <Row className="contentLogos">
            <Col xs={6} md={4} lg={3} className="d-flex justify-content-center text-center">
                <a href="https://iwof.com.br/" className="text-decoration-none">
                    <img src={Iwof} alt="iwof" loading="lazy" />
                </a>
            </Col>
            <Col xs={6} md={4} lg={3} className="d-flex justify-content-center text-center">
                <a href="https://bucado.com.br/" className="text-decoration-none">
                    <img src={Bucado} alt="bucado" loading="lazy" />
                </a>
            </Col>
        </Row>
        
  </Container>
);

export default StartupsLogos;
