import React, { Component } from 'react'

import { Container, Row, Col, Button } from 'react-bootstrap';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import { sendContato } from '../../app/actions/ContactsAction';

import './styles.scss';

class FormBuy extends Component{
    constructor(props){
        super(props)

        this.state = {
            name: '',
            startup: '',
            email: '',
            phone: '',
            message: '',
            file: '',
        }

        this.method = 'send-contact'
        this.handleFielsChange = this.handleFielsChange.bind(this)
        this.handleSendMail = this.handleSendMail.bind(this)
        this.clearForm = this.clearForm.bind(this)
        this.onChangeHandler = this.onChangeHandler.bind(this)

    }

    onChangeHandler(event){
        this.setState({
            file: event.target.files[0],
        })
    }

    handleFielsChange(event) {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    }

    handleSendMail(event) {
        event.preventDefault();
        this.setState({ buttonEnabled:false, buttonText: "Aguarde" })
		console.log(this.state)
		const formData = new FormData()
		formData.append('name', this.state.name)
        formData.append('startup', this.state.startup)
		formData.append('phone', this.state.phone)
		formData.append('email', this.state.email)
		formData.append('message', this.state.message)
		formData.append('file', this.state.file)

		this.props.sendContato(this.method, formData)
		// this.clearForm()
    }

    clearForm() {
        this.setState({
            name: "",
            phone: "",
            email: "",
			message: "",
            file: ""
        })
    }

    render(){
        return (

            <form className="FormBuy" id="formplano" onSubmit={this.handleSendMail} encType="multipart/form-data">
                <Container>
                    <Row>
                        <Col sm={12} lg={12}>
                            <h3 className="FormBuy__title">
                                Fale sobre sua startup
                            </h3>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} lg={6}>
                            <div className="FormBuy__containerInput">
                                <label className="FormBuy__containerInput--label">Informe seu nome</label>
                                <input type="text" name="name" className="FormBuy__containerInput--input" value={this.state.name} onChange={this.handleFielsChange} />
                            </div>
                        </Col>
                        <Col sm={12} lg={6}>
                            <div className="FormBuy__containerInput">
                                <label htmlFor="startup" className="FormBuy__containerInput--label">Informe a sua startup</label>
                                <input type="text" name="startup" className="FormBuy__containerInput--input" value={this.state.startup} onChange={this.handleFielsChange} />
                            </div>
                        </Col>
                        <Col sm={12} lg={6}>
                            <div className="FormBuy__containerInput">
                                <label htmlFor="startup" className="FormBuy__containerInput--label">Informe seu e-mail</label>
                                <input type="email" name="email" id="startup" className="FormBuy__containerInput--input" value={this.state.email} onChange={this.handleFielsChange} />
                            </div>
                        </Col>
                        <Col sm={12} lg={6}>
                            <div className="FormBuy__containerInput">
                                <label htmlFor="startup" className="FormBuy__containerInput--label">Informe seu telefone</label>
                                <input type="tel" name="phone" className="FormBuy__containerInput--input" value={this.state.phone} onChange={this.handleFielsChange} />
                            </div>
                        </Col>
                        <Col sm={12} lg={12}>
                            <div className="FormBuy__containerInput">
                                <label htmlFor="startup" className="FormBuy__containerInput--label">Fale um pouco sobre sua startup</label>
                                <textarea type="text" name="message" className="FormBuy__containerInput--input" rows="5" value={this.state.message} onChange={this.handleFielsChange} />
                            </div>
                        </Col>
                        <Col sm={12} lg={12}>
                            <div className="FormBuy__containerInput">
                                <label htmlFor="startup" className="FormBuy__containerInput--label">Anexar o Pitch</label>
                                <input type="file" name="file" className="FormBuy__containerInput--input" onChange={this.onChangeHandler} />
                            </div>
                        </Col>
                        <Col className="d-flex justify-content-end pt-5">
                            <Button type="submit" variant="dark" className="px-5 py-3 rounded-0">Enviar</Button>
                        </Col>
                    </Row>
                </Container>
            </form>
        )
    }
}


const mapDispatchToProps = (dispatch) => bindActionCreators({sendContato}, dispatch)
export default connect(null, mapDispatchToProps) (FormBuy);
