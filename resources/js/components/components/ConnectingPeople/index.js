import { Container, Row, Col } from 'react-bootstrap';

import ImageConnecting from '../ImageConnecting';
import Slider from "react-slick";
import styled from 'styled-components'


import './styles.scss';
 
const ComponentImg = styled.div`
	background-image: url('${props => props.Value }');
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
    height: 600px;
    width: 100%;

	@media(max-width: 768px){
		height: 300px;
	}
`

const settings = {
	dots: true,
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay: true,
	speed: 500,
	autoplaySpeed: 5000,
	cssEase: "linear",
	pauseOnHover: true
};

const Imgs = [
	{img: '/images/home/1.png'},
	{img: '/images/home/2.png'},
	{img: '/images/home/3.png'},
	{img: '/images/home/4.png'},
	{img: '/images/home/5.png'},
	{img: '/images/home/6.png'}
]	


const ConnectingPeople = () => (
	<Container>
		<Row className="containerConnectingPeople">
			<Col lg={5} sm={12}>
				<h3 className="containerConnectingPeople__h3">
					Conectando pessoas
				<br className="d-none d-lg-block" />
					{' '}
					e empresas
				</h3>
			</Col>
			<Col lg={7} sm={12}>
				<p className="containerConnectingPeople__p">
					Não somos um coworking, somos um ecossistema de empreendedorismo tecnológico. Iremos conectar startups a grandes empresas, pesquisadores, investidores e desenvolvedores.
				</p>
			</Col>
		</Row>
		<div className="d-lg-none">
			<Slider {...settings}>
				{Imgs.map((item, key) => {
					return(
						<div key={key}>
							<a  data-fancybox="gallery" href={item.img}>
								<ComponentImg Value={item.img}>
									<img src={item.img} className="d-none" />
								</ComponentImg>
							</a>
						</div>
					)
				})}	
			</Slider>
		</div>
		<div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
		<ol className="carousel-indicators">
			<li data-target="#carouselExampleControls" data-slide-to="0" className="active m-2"></li>
			<li data-target="#carouselExampleControls" data-slide-to="1" className="m-2"></li>
		</ol>
			<div className="carousel-inner">
				<div className="carousel-item active">
					<Row className="d-none d-lg-flex">
						<Col lg={6} className="imagesDesktop__image--first" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/sala4.jpg`}>
								<ImageConnecting image={`/images/sala4.jpg`} />
							</a>
						</Col>
						<Col lg={3} className="imagesDesktop__image--second" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/sala5.jpg`}>
								<ImageConnecting image={`/images/sala5.jpg`} />
							</a>
						</Col>
						<Col lg={3} className="imagesDesktop__image" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/sala6.jpg`}>
								<ImageConnecting image={`/images/sala6.jpg`} />
							</a>
						</Col>
						<Col lg={3} className="imagesDesktop__image--first" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/home/4.png`}>
								<ImageConnecting image={`/images/home/4.png`} />
							</a>
						</Col>
						<Col lg={3} className="imagesDesktop__image--second" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/home/5.png`}>
								<ImageConnecting image={`/images/home/5.png`} />
							</a>
						</Col>
						<Col lg={6} className="imagesDesktop__image" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/home/6.png`}>
								<ImageConnecting image={`/images/home/6.png`} />
							</a>
						</Col>
					</Row>
				</div>
				<div className="carousel-item">
					<Row className="d-none d-lg-flex">
						<Col lg={6} className="imagesDesktop__image--first" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/sala1.jpg`}>
								<ImageConnecting image={`/images/sala1.jpg`} />
							</a>
						</Col>
						<Col lg={3} className="imagesDesktop__image--second" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/sala2.jpg`}>
								<ImageConnecting image={`/images/sala2.jpg`} />
							</a>
						</Col>
						<Col lg={3} className="imagesDesktop__image" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/sala3.jpg`}>
								<ImageConnecting image={`/images/sala3.jpg`} />
							</a>
						</Col>
						<Col lg={3} className="imagesDesktop__image--first" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/home/1.png`}>
								<ImageConnecting image={`/images/home/1.png`} />
							</a>
						</Col>
						<Col lg={3} className="imagesDesktop__image--second" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/home/2.png`}>
								<ImageConnecting image={`/images/home/2.png`} />
							</a>
						</Col>
						<Col lg={6} className="imagesDesktop__image" style={{ paddingBottom: '2px' }}>
							<a data-fancybox="gallery" href={`/images/home/3.png`}>
								<ImageConnecting image={`/images/home/3.png`} />
							</a>
						</Col>
					</Row>
				</div>
			</div>
		</div>
	</Container>
);

export default ConnectingPeople;