
import KnowPlans from '../../components/KnowPlans';
import BePartOfIt from '../../components/BePartOfIt';
import FormBuy from '../../components/FormBuy';
import Layout from '../Layout';

export default function Part() {
	return (
		<Layout>
			<BePartOfIt />
			<KnowPlans />
			<FormBuy />
		</Layout>
	);
}
