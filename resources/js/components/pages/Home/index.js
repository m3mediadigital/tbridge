import React from 'react'

import Banner from '../../components/Banner'
import CheckTheSchedule from '../../components/CheckTheSchedule';
import ConnectingPeople from '../../components/ConnectingPeople';
import Layout from '../Layout';
import WeWantToKnow from '../../components/WeWantToKnow';

export default function Home() {
	return (
		<Layout>
			<Banner />
			<ConnectingPeople />
			<WeWantToKnow />
			<CheckTheSchedule />
		</Layout>
	);
}
