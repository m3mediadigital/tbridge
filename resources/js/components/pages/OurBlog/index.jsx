import React, { Component } from 'react'

import OurBlogComponent from '../../components/OurBlog';
import Layout from '../Layout';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { listBlogsList } from '../../app/actions/BlogsAction';


class OurBlog extends Component{
	constructor(props){
		super(props)
	
		this.listBlogsList()
	}

	listBlogsList(){
		this.props.listBlogsList()
	}

	render(){
		return (
			<Layout>
				<OurBlogComponent items={this.props.list_blogs} />
			</Layout>
		)
	}
}

const mapStateToProps = state => ({
    list_blogs: state.blogs.get('list_blogs')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        listBlogsList
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(OurBlog)