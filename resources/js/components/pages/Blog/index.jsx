import React, { Component } from 'react'

import InternaBlog from '../../components/OurBlog/interna';
import Layout from '../Layout';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getBlog } from '../../app/actions/BlogsAction';
import { listBlogs } from '../../app/actions/BlogsAction';

import Loader from 'react-loader-spinner'

class Blog extends Component{
	constructor(props){
		super(props)
	
		this.props.getBlog(this.props.match.params.slug)
		this.listBlogs()
	}

	listBlogs(){
		this.props.listBlogs()
	}

	getBlogs(){
		this.props.getBlogs()
	}

	render(){
		const {item_blog} = this.props
		const blog = item_blog != null ? item_blog.toJS() : null;
		return (
			
			<Layout>
				{
					item_blog !== null && item_blog.size === 0 ?
					<div className="pt-5 d-flex justify-content-center">
						<Loader className="pt-5"
							type="Oval" 
							color="#000"
							height={100}
							width={100}
						/>
					</div>
					: item_blog && (<InternaBlog item={blog} />)
				}
			</Layout>
		)
	}
}

const mapStateToProps = state => ({
    item_blog: state.blogs.get('item_blog'),
    list_blog: state.blogs.get('list_blog')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        getBlog,
		listBlogs
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Blog)