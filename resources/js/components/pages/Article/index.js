
import ArticleComponent from '../../components/Article';
import Layout from '../Layout';

export default function OurBlog() {
	return (
		<Layout>
			<ArticleComponent />
		</Layout>
	);
}
