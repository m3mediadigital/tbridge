import React, {Component} from 'react'
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';

import { getSettings } from '../app/actions/SettingsAction'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
class Layout extends Component{
    constructor(props){
        super(props)
        this.getSettings()
    }

    getSettings(){
        this.props.getSettings()
    }

    render(){
        return (
            <React.Fragment>
                <Navbar/>
                {this.props.children}
                <Footer settings={this.props.list_settings} />
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    list_settings: state.settings.get('list_settings')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        getSettings
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Layout)
