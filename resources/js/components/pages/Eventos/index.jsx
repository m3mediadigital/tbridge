import React, { Component } from 'react'

import { Container, Row, Col } from 'react-bootstrap';

import InternaEventos from '../../components/CheckTheSchedule/interna';
import Layout from '../Layout';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { listEvents } from '../../app/actions/EventsAction';

class Events extends Component{
	constructor(props){
		super(props)
		this.listEvents()
	}

	listEvents(){
		this.props.listEvents()
	}

	render(){
		const { list_events } = this.props
		return(
			<Layout>
				<div className="OurBlog" id="eventosId">
					<Container>
						<Row>
							<Col sm={12} lg={12}>
								<h1 className="OurBlog__title">Eventos</h1>
							</Col>
						</Row>
						{
						list_events != null && list_events.size === 0 
							? false : (
								list_events.map((item, key) => {
									return <InternaEventos events={item} key={key} />
								})
							)
						}
					</Container>
				</div>
				
			</Layout>
		)
	}
}

const mapStateToProps = state => ({
    list_events: state.events.get('list_events')
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
		listEvents
    }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Events)