@component('mail::message')
# Nova mensagem

{{$item->name}}({{$item->email}}) enviou um novo pitch!

Clique no botão para ver a mensagem.

@component('mail::button', ['url' => route('dashboard.contacts.show',$item['id'])])
Clique aqui
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
