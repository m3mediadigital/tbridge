@component('mail::message')
# Nova mensagem

{{$item->name}}
<br/>
{{$item->phone}}
<br/>
{{$item->email}}
<br/>
{{$item->message}}
<br/>
{{$item->startup}}


Obrigado,<br>
{{ config('app.name') }}
@endcomponent
