<x-dashboard>
    <x-slot name="title">{{ __('Eventos / '.$item->title.' / Galeria') }}</x-slot>
    <x-slot name="header">
        {{-- @include('layouts.headers.cards') --}}
    </x-slot>
    <x-slot name="css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
        <style>
            .gallery{
                height: 200px;
            }
        </style>
    </x-slot>
    <x-slot name="content"> 
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="card w-100">
                <!-- Card header -->
                <div class="card-header border-0">

                    <div class="row">
                        <div class="col-12 col-sm-3 col-md-3 col-lg-4 col-xl-7">
                            <h3 class="mb-0">Adicionar Fotos do Evento (Precisa refatorar)</h3>
                        </div>
                    </div>
                </div>
                
                <div class="card-body">
                    <form method="post" action="{{ route('dashboard.event.galeria') }}" enctype="multipart/form-data"
                        class="dropzone" id="my-dropzone">
                        <input type="hidden" name="event" value="{{ $item->id }}">
                        @csrf
                    </form>

                    <div class="row pt-5">
                        {{--  @foreach ($gallerys as $item)
                            <div class="col-12 col-sm-6 col-md-3 col-lg-3 pb-4">
                                {{ deleteButton(route('ae.galeria.destroy', $item->id)) }}
                                <img src="{{ Storage::url($item->filename) }}" class="w-100 img-fluid gallery"
                                    data-dz-remove>
                            </div>
                        @endforeach  --}}
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
        <script type="text/javascript">
            const id = "{{ $item->id }}"
            Dropzone.options.dropzone = {
                maxFilesize: 1,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time + file.name;
                },
                acceptedFiles: ".jpeg,.jpg,.png",
                addRemoveLinks: true,
                timeout: 50000,
                removedfile: function(file) {
                    var name = file.upload.filename;
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        type: 'POST',
                        url: '/dashboard/event/galeria',
                        data: {
                            filename: name, id: id
                        },
                        success: function(data) {
                            console.log("File has been successfully removed!!");
                        },
                        error: function(e) {
                            console.log(e)
                        }
                    });
                    var fileRef;
                    return (fileRef = file.previewElement) != null ?
                        fileRef.parentNode.removeChild(file.previewElement) : void 0;
                },

                success: function(file, response) {
                    console.log(response);
                },
                error: function(file, response) {
                    alert('error')
                }
            }
        </script>
    </x-slot>
</x-dashboard>