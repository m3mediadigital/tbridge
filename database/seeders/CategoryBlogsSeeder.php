<?php

namespace Database\Seeders;

use App\Models\CategoryBlogs;
use Illuminate\Database\Seeder;

class CategoryBlogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['title' => 'Lançamento', 'color' => '#EF7C32'],
            ['title' => 'Negócios', 'color' => '#8D19F7'],
            ['title' => 'Ecosistema', 'color' => '#77B926']
        ];

        foreach($items as $item){
            CategoryBlogs::create($item);
        }
    }
}
