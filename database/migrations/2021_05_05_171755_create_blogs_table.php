<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('category_blogs_id')->nullable();
            $table->foreign('category_blogs_id')->references('id')->on('category_blogs')->onDelete('cascade');
            $table->boolean('active');
            $table->string('title');
            $table->text('autor')->nullable();
            $table->text('tags')->nullable();
            $table->text('caption')->nullable();
            $table->text('seo')->nullable();
            $table->text('description');
            $table->string('slug');
            $table->date('publish_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
