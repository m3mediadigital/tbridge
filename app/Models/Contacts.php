<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = "contacts";

    public function files()
    {
        return $this->belongsTo('App\Models\Files');
    }
}
