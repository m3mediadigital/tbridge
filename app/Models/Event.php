<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Event extends Model
{
    use Sluggable;

    protected $fillable = [
        'files_id',
        'title',
        'link',
        'description',
        'content',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function files()
    {
        return $this->belongsTo('App\Models\Files');
    }

     public static function AllCustomEvents()
    {
        return self::query()
            ->select(
                '*'
            )->with([
                'files' => function ($q){
                    $q->select(
                        'id',
                        'path'
                    );
                }
            ])->orderBy('created_at','desc')->get();
    }

    public static function AllCustomEventsList()
    {
        return self::query()
            ->select(
                '*'
            )->with([
                'files' => function ($q){
                    $q->select(
                        'id',
                        'path'
                    );
                },
                'categorys' => function($q){
                    $q->select(
                        'id',
                        'title',
                        'color'
                    );
                }
            ])->orderBy('created_at','desc')->get();
    }

    public static function AllCustomEventsSlug($slug)
    {
        return self::query()
            ->select(
                '*'
            )->with([
                // 'files' => function ($q){
                //     $q->select(
                //         'id',
                //         'path'
                //     );
                // },
                'categorys' => function($q){
                    $q->select(
                        'id',
                        'title',
                        'color'
                    );
                }
            ])->where([['active', 1],['publish_at','<=', now()->format('Y-m-d')],['slug', $slug]])->first();
    }
}
