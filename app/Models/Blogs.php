<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\DB;

class Blogs extends Model
{
    use HasFactory, Sluggable;

    protected $table = "blogs";
    protected $fillable = [
        'files_id',
        'category_blogs_id',
        'title',
        'autor',
        'tags',
        'caption',
        'seo',
        'description',
        'publish_at',
        'active'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function files()
    {
        return $this->belongsTo('App\Models\Files');
    }

    public function categorys()
    {
        return $this->belongsTo(CategoryBlogs::class,'category_blogs_id');
    }

    public static function AllCustomBlogs()
    {
        return self::query()
            ->select(
                '*'
            )->with([
                'files' => function ($q){
                    $q->select(
                        'id',
                        'path'
                    );
                },
                'categorys' => function($q){
                    $q->select(
                        'id',
                        'title',
                        'color'
                    );
                }
            ])->where([['active', 1],['publish_at','<=', now()->format('Y-m-d')]])->limit(3)->orderBy('publish_at','desc')->get();
    }

    public static function AllCustomBlogsList()
    {
        return self::query()
            ->select(
                '*'
            )->with([
                'files' => function ($q){
                    $q->select(
                        'id',
                        'path'
                    );
                },
                'categorys' => function($q){
                    $q->select(
                        'id',
                        'title',
                        'color'
                    );
                }
            ])->where([['active', 1],['publish_at','<=', now()->format('Y-m-d')]])->orderBy('publish_at','desc')->get();
    }

    public static function AllCustomBlogsSlug($slug)
    {
        return self::query()
            ->select(
                '*'
            )->with([
                'files' => function ($q){
                    $q->select(
                        'id',
                        'path'
                    );
                },
                'categorys' => function($q){
                    $q->select(
                        'id',
                        'title',
                        'color'
                    );
                }
            ])->where([['active', 1],['publish_at','<=', now()->format('Y-m-d')],['slug', $slug]])->first();
    }
}
