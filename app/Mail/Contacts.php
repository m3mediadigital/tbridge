<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contacts extends Mailable
{
    public $item;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($item)
    {
        $this->item = $item;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(!empty($this->item['files_id'])){
            $subject = "Novo Pitch";
            $template = 'emails.pitch';
        } else {
            $subject = "Novo Contato";
            $template = 'emails.contacts';
        }

        return $this->markdown($template)->subject($subject);
    }
}
