<?php

namespace App\Http\Livewire;

use App\Models\Event as ModelEvent;
use Livewire\Component;
use Livewire\WithPagination;

class Event extends Component
{
    use WithPagination;

    public $search;
    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';


    public function render()
    {
        return view('livewire.event',[
            'items' => ModelEvent::where('title', 'like', '%'.$this->search.'%')
                        ->orderBy('created_at','asc')
                            ->paginate(15)
        ]);
    }
}
