<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Files;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{

    public function __invoke(Request $request)
    {
        $this->galeria($request);
        
        return response()->json(['success','Imagem cadastrado com sucesso!']);
        return back();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.event.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->file){
            $file = new Files;

            try {
                $file->upload($file, $request->file);
            } catch (\Throwable $th) {
                throw $th;
                Log::error($th);
                return redirect()->back()->with('error','Erro ao cadastrar imagem!');
            }

            $request['files_id'] = $file->id;
        }

        try {
            Event::create($request->all());
        } catch (\Throwable $th) {
            throw $th;
            Log::error($th);
            return redirect()->back()->with('error','Erro ao cadastrar Evento!');
        }

        return redirect()->route('dashboard.event.index')->with('success','Evento Cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return view('dashboard.event.show',[
            'item' => $event
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('dashboard.event.edit',[
            'item' => $event
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        if($request->file):
            $file = new Files;

            try {
                Storage::delete($event->files->filename);
                $file->upload($file, $request->file);
            } catch (\Throwable $th) {
                throw $th;
                return redirect()->back()->with('error','Erro ao atualizar imagem!');
            }

            $request['files_id'] = $file->id;

        endif;

        try {
            $event->update($request->all());
        } catch (\Throwable $th) {
            // throw $th;
            Log::error($th);
            return redirect()->back()->with('error','Erro ao atualizar Evento!');
        }

        return redirect()->route('dashboard.event.index')->with('success','Evento atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function galeria(Request $request): void {
        // dd($request->all());
        // $even
        // $file = new Files;
        // $file->gallery_id = auth()->user()->companies->gallery_id;    
    
        // $file->upload($file,$request->file, auth()->user()->companies->slug);
        // $file->save();

        
    }
}
