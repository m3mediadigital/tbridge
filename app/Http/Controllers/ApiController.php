<?php

namespace App\Http\Controllers;

use App\Mail\Contacts as MailContacts;
use App\Models\Blogs;
use App\Models\Event;
use App\Models\Contacts;
use App\Models\Files;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ApiController extends Controller
{
    public $settings;

    public function __construct()
    {
        $this->settings = Settings::select('description','slug')->pluck('description','slug')->all();

        view()->share('settings', $this->settings);
    }

    public function settings()
    {
        return response()->json($this->settings);
    }

    public function contact(Request $request)
    {
        $send = $this->settings['email-contato'];

        $contact = new Contacts;
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->message = $request->message;
        if(isset($request->startup) && !empty($request->startup)):
            $contact->startup = $request->startup;
        endif;

        if(isset($request->file) && !empty($request->file)):
            $file = new Files;
            try {
                $file->uploadFile($file, $request->file);
            } catch (\Throwable $th) {
                throw $th;
            }

            $contact->files_id = $file->id;

            $send = $this->settings['email-leads'];
        endif;

        try {
            $contact->save();
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['error'=> 'Por favor tente novamente.','warning' => $th]);
        } finally {

            Mail::to($send)->send(new MailContacts($contact));

        }

        return response()->json(['success' => 'Em breve entraremos em contato.']);
    }

    public function blogs()
    {
        return response()->json(Blogs::AllCustomBlogs());
    }

    public function blog($slug)
    {

        return response()->json(Blogs::AllCustomBlogsSlug($slug));
    }

    public function blogsList()
    {
        return response()->json(Blogs::AllCustomBlogsList());
    }

    public function events()
    {
        return response()->json(Event::AllCustomEvents());
    }
}
